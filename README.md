# How to create animated 3D objects with blender

## Installation

First of the first, you need to install blender 2.93

```
wget https://ftp.nluug.nl/pub/graphics/blender/release/Blender2.93/blender-2.93.0-linux-x64.tar.xz
```
```
tar -xvf blender-2.93.0-linux-x64.tar.xz
```
```
cd blender-2.93.0*release
```
```
./blender
```

In blender, add a new add-on

Go in *Edit/Preferences* -> *Add-ons*, then click *Install...*

![Blender Add adds-on](img/blenderAddAddsOn.png)

Select the python script for blender 2.93 from [here](https://github.com/lsalzman/iqm)

Search `iqm` and activate the add-on

![Blender Activate add-on](img/blenderActivateAddOn.png)

## Modelisation

First of all, you need to have a 3D object (mesh)

![Blender 3D object](img/blender3DObject.png)

Then, create the armature you want, edit it in *Edit mode*

![Blender Armature](img/blenderArmature.png)

Link the armature to the mesh
- In *object mode*
- LeftClick on the mesh
- Shift+LeftClick on the armature
- Ctrl+p
- Select *Armature Deform/With Automatic Weights*

Make animations in 60 fps

![Blender 60 FPS](img/blender60fps.png)

Pass in *Dope Sheet* mode

![Blender Dope Sheet Mode](img/blenderDopeSheetMode.png)

And then in *Action Editor*

![Blender Action Editor](img/blenderActionEditor.png)

Click *new*

![Blender New Animation](img/blenderNewAnimation.png)

Create as many animation as you want by cloning the first  
Once you have created all your animations, clone the last created one, this new animation will be the one unused  
Delete this animation

![Blender Delete Animation](img/blenderDeleteAnimation.png)

And add *Fake User* on all the others

![Blender Fake User](img/blenderFakeUser.png)

You should have something like this, the **0** one is the deleted and all others should be **F**

![Blender Animations Recap](img/blenderAnimationsRecap.png)

For each animation, position at first frame

![Blender first frame](img/blenderAnimation1Frame.png)

In *Pose mode*, select all bones by pressing *A*  
Then press *I* and select *Location & Rotation*

![Blender Location and Rotation](img/blenderLocationAndRotation.png)

At each frame you want a different position, move your bones, and then repeat the previous actions

Now you have your animations, you can save the models with the animations has an *.iqm* file  

Select the model and the armature:
- LeftClick on the mesh
- Shift+LeftClick on the armature

Click on *File/Export/Inter-Quake Model*

![Blender Export Menu](img/blenderExportMenu.png)

When exporting, give your model a name, and **DON'T FORGET TO SPECIFY THE ANIMATIONS**  
Animations specify are just the names of the animations comma-seperated

![Blender Exporting](img/blenderExporting.png)

All done !

---

You can test your models with the given code  
Just edit the macros at the start of `Main.cpp` and then run
```
./build.sh && ./raylibtest
```
