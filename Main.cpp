#include <raylib.h>

#include <string>

#define MODEL_PATH "./botte.iqm"
#define MODEL_TEXTURE_PATH "./texture.png"
#define CAMERA_HEIGHT 10

int main() {
    InitWindow(GetScreenWidth(), GetScreenHeight(), "Raylib test");

    Camera3D camera = {0};
    camera.position = (Vector3){.x = 10, .y = CAMERA_HEIGHT, .z = 10};
    camera.target = (Vector3){.x = 0, .y = 0, .z = 0};
    camera.up = (Vector3){.x = 0, .y = 1, .z = 0};
    camera.fovy = 45;
    camera.projection = CAMERA_PERSPECTIVE;

    Model model = LoadModel(MODEL_PATH);
    Texture2D texture = LoadTexture(MODEL_TEXTURE_PATH);
    SetMaterialTexture(&model.materials[0], MATERIAL_MAP_DIFFUSE, texture);

    Vector3 modelPosition = {.x = 0, .y = 0, .z = 0};
    Vector3 modelScale = {.x = 1, .y = 1, .z = 1};
    Vector3 modelRotationAxis = {.x = 1, .y = 0, .z = 0};

    bool animationActive = false;
    bool looping = false;
    int animationsCount = 0;
    int animationFrameCounter = 0;
    int usedAnimation = 0;
    ModelAnimation *modelAnimations;
    modelAnimations = LoadModelAnimations(MODEL_PATH, &animationsCount);

    SetCameraMode(camera, CAMERA_ORBITAL);
    SetTargetFPS(60);

    while (not WindowShouldClose()) {
        UpdateCamera(&camera);

        if (animationsCount > 0 and IsKeyPressed(KEY_ENTER)) {
            looping = not looping;
        }
        if (animationsCount > 0 and IsKeyPressed(KEY_SPACE)) {
            animationActive = not animationActive;
            animationFrameCounter = 0;
        }
        if (animationsCount > 0 and IsKeyPressed(KEY_LEFT)) {
            if (not looping) {
                animationActive = false;
            }
            animationFrameCounter = 0;
            usedAnimation--;
            if (usedAnimation < 0) {
                usedAnimation = animationsCount - 1;
            }
        }
        if (animationsCount > 0 and IsKeyPressed(KEY_RIGHT)) {
            if (not looping) {
                animationActive = false;
            }
            animationFrameCounter = 0;
            usedAnimation++;
            if (usedAnimation >= animationsCount) {
                usedAnimation = 0;
            }
        }
        if (animationActive and animationsCount > 0) {
            UpdateModelAnimation(model, modelAnimations[usedAnimation],
                                 animationFrameCounter);
            animationFrameCounter++;
            if (animationFrameCounter >
                modelAnimations[usedAnimation].frameCount) {
                if (looping) {
                    animationFrameCounter -=
                        modelAnimations[usedAnimation].frameCount;
                } else {
                    animationActive = false;
                }
            }
        }

        BeginDrawing();
        {
            ClearBackground(BLACK);

            BeginMode3D(camera);
            {
                DrawModelEx(model, modelPosition, modelRotationAxis, 0,
                            modelScale, WHITE);

                DrawGrid(20, 1);
            }
            EndMode3D();

            DrawFPS(10, 10);
            DrawText("PRESS SPACE TO PLAY MODEL ANIMATION", 10, 30, 20, WHITE);
            if (animationsCount > 0) {
                DrawText(std::string{"ANIMATION "}
                             .append(std::to_string(usedAnimation + 1))
                             .append(" IS SELECTED")
                             .c_str(),
                         10, 50, 20, WHITE);
                if (animationActive) {
                    DrawText(std::string{"ANIMATION IS ACTIVE"}.c_str(), 10, 70,
                             20, GREEN);
                } else {
                    DrawText(std::string{"ANIMATION IS INACTIVE"}.c_str(), 10,
                             70, 20, RED);
                }
                if (looping) {
                    DrawText(std::string{"ANIMATION LOOPING IS ENABLE"}.c_str(),
                             10, 90, 20, GREEN);
                } else {
                    DrawText(
                        std::string{"ANIMATION LOOPING IS DISABLE"}.c_str(), 10,
                        90, 20, RED);
                }
            } else {
                DrawText("MODEL DOES NOT CONTAIN ANIMATIONS", 10, 50, 20,
                         WHITE);
            }
        }
        EndDrawing();
    }

    UnloadTexture(texture);

    if (animationsCount > 0) {
        UnloadModelAnimations(modelAnimations, animationsCount);
    }

    UnloadModel(model);

    CloseWindow();

    return 0;
}
