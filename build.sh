#!/bin/bash

args=""

if [[ "$*" == *"re"* ]] ; then
    rm -rf build
fi

mkdir -p ./build && cd ./build

if [[ "$*" == *"cc"* ]] ; then
    args="$args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
fi

if [[ "$*" == *"debug"* ]] ; then
    cmake .. -G "Ninja" -DCMAKE_BUILD_TYPE=Debug $args
elif [[ "$*" == *"release"* ]] ; then
    cmake .. -G "Ninja" -DCMAKE_BUILD_TYPE=Release $args
else
    cmake .. -G "Ninja" $args
fi

cmake --build .

if [[ "$*" == *"cc"* ]] ; then
    mv compile_commands.json ..
fi
